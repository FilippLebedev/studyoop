//
//  ModuleConfigurator.swift
//  StudyOOP
//
//  Created by Filipp Lebedev on 07.09.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation
import UIKit

final class ModuleConfigurator {
    
//    func configureViewController<T>(ofType type: T) -> UIViewController? {
//        switch type {
//
//        case is StartViewController: return createStart()
//        case is WorkTimeViewController: return createWorkTime()
//
//        default: return nil
//        }
//    }
    
    private func createView<T>(ofType type: T) -> UIViewController {
        let identifier = String(describing: type.self)
        let storyboard = UIStoryboard(name: identifier, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: identifier)
        return viewController
    }
    
    func createStart() -> StartViewController {
        
        let view = createView(ofType: StartViewController.self) as! StartViewController
        let presenter = StartPresenter()
        let interactor = StartInteractor()
        let router = StartRouter()
        
        view.output = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        
        return view
    }
    
    func createWorkTime() -> WorkTimeViewController {
        
        let view = createView(ofType: WorkTimeViewController.self) as! WorkTimeViewController
        let presenter = WorkTimePresenter()
        let interactor = WorkTimeInteractor()
        let router = WorkTimeRouter()
        
        view.output = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        
        return view
    }
}
