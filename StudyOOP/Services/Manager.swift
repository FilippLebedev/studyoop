//
//  Manager.swift
//  StudyOOP
//
//  Created by Filipp Lebedev on 07.09.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation
import UIKit

final class Manager {
    
    static let shared = Manager()
    
    private init () {}

    private(set) var moduleConfigurator: ModuleConfigurator!
    
    private var isConfigured = false
    
    var navigationController: UINavigationController?
    
    func configure() {
        guard !isConfigured else { return }
        moduleConfigurator = ModuleConfigurator()
        isConfigured = true
    }
    
    func pushViewController(_ vc: UIViewController) {
        navigationController?.pushViewController(vc, animated: true)
    }
}
