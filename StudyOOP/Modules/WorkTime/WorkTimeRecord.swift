//
//  WorkTimeRecord.swift
//  StudyOOP
//
//  Created by Filipp Lebedev on 06.09.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation

class WorkTimeRecord {
    
    var startTime: Date
    var duration: TimeInterval
    
    init(startTime: Date, duration: TimeInterval) {
        self.startTime = startTime
        self.duration = duration
    }
}

class WorkTimeRecordGroup {
    
    var records: [WorkTimeRecord] = []
    var interval: WorkTimeInterval
    
    var timeAmount: TimeInterval {
        return records.map({$0.duration}).reduce(0, +)
    }
    
    init(interval: WorkTimeInterval) {
        self.interval = interval
    }
    
    func appendIfIncludeInterval(record: WorkTimeRecord) {
        if record.startTime >= interval.minimalDate && record.startTime <= interval.maximalDate {
            records.append(record)
        }
    }
}

class WorkTimeRecordGroupDateRepresentation {
    
    private var formatter = DateFormatter()
    
    init(dateFormat: String) {
        formatter.dateFormat = dateFormat
    }
    
    func representativeDate(for date: Date) -> String {
        return formatter.string(from: date)
    }
}

class WorkTimeRecordGroupManager {
    
    var groups: [WorkTimeRecordGroup] = []
    var range: WorkTimeIntervalRange
    
    var timeAmount: TimeInterval {
        return groups.map({$0.timeAmount}).reduce(0, +)
    }
    
    func update(records: [WorkTimeRecord], completion: ([WorkTimeRecordGroup]) -> ()) {
        groups = []
        
        for interval in range.intervals {
            let newGroup = WorkTimeRecordGroup(interval: interval)
            for record in records {
                newGroup.appendIfIncludeInterval(record: record)
            }
            groups.append(newGroup)
        }
        
        completion(groups)
    }
    
    init(range: WorkTimeIntervalRange) {
        self.range = range
    }
}

