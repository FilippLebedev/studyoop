//
//  WorkTimeDataSource.swift
//  StudyOOP
//
//  Created by Filipp Lebedev on 06.09.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation

class WorkTimeDataSource {
    
    private var groupDateRepresentation: WorkTimeRecordGroupDateRepresentation?
    private var data = [WorkTimeRecordGroup]()
    
    var intervalQuantity: Int {
        return data.count
    }
    
    func timeAmountForInterval(at index: Int) -> TimeInterval {
        guard index >= 0, index < intervalQuantity else { return 0 }
        return data[index].timeAmount
    }
    
    func maximalTimeAmountForInterval(at index: Int) -> TimeInterval {
        guard index >= 0, index < intervalQuantity else { return 0 }
        return data[index].interval.maximalDuration
    }
    
    func minimalDateForInterval(at index: Int) -> Date {
        guard index >= 0, index < intervalQuantity else { return Date() }
        return data[index].interval.minimalDate
    }
    
    func representativeDateForInterval(at index: Int) -> String {
        guard index >= 0, index < intervalQuantity else { return "" }
        return groupDateRepresentation?.representativeDate(for: minimalDateForInterval(at: index)) ?? ""
    }
    
    func update(data: [WorkTimeRecordGroup]) {
        self.data = data
    }
    
    func setDateFormat(format: String) {
        groupDateRepresentation = WorkTimeRecordGroupDateRepresentation(dateFormat: format)
    }
}
