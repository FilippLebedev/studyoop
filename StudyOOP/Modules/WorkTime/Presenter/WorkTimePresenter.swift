//
//  WorkTimeWorkTimePresenter.swift
//  StudyOOP
//
//  Created by Filipp Lebedev on 06/09/2018.
//  Copyright © 2018 FilippLebedev. All rights reserved.
//

import UIKit

class WorkTimePresenter: WorkTimeModuleInput, WorkTimeViewOutput {

    weak var view: WorkTimeViewInput!
    var interactor: WorkTimeInteractorInput!
    var router: WorkTimeRouterInput!
    
    private var groupManager: WorkTimeRecordGroupManager? = nil

    func viewIsReady() {
        let intervalRangeTemplate = WorkTimeIntervalRangeTemplate(intervalDuration: 86400, intervalQuantity: 7)
        let startDate = Date().addingTimeInterval(-86400 * 7)
        let intervalRange = WorkTimeIntervalRange(by: intervalRangeTemplate, startTime: startDate)
        self.groupManager = WorkTimeRecordGroupManager(range: intervalRange)
        
        interactor.setup()
    }
}

extension WorkTimePresenter: WorkTimeInteractorOutput {
    
    func update(records: [WorkTimeRecord]) {
        groupManager?.update(records: records) { (results) in
            view.update(records: results)
        }
    }
}
