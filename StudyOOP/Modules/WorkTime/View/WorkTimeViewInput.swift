//
//  WorkTimeWorkTimeViewInput.swift
//  StudyOOP
//
//  Created by Filipp Lebedev on 06/09/2018.
//  Copyright © 2018 FilippLebedev. All rights reserved.
//

protocol WorkTimeViewInput: class {

    func update(records: [WorkTimeRecordGroup])
}
