//
//  WorkTimeWorkTimeViewController.swift
//  StudyOOP
//
//  Created by Filipp Lebedev on 06/09/2018.
//  Copyright © 2018 FilippLebedev. All rights reserved.
//

import UIKit
import Cartography

class WorkTimeViewController: UIViewController {

    var output: WorkTimeViewOutput!
    var dataSource: WorkTimeDataSource? = nil
    var viewCalendar = WorkTimeCalendarView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareDataSource()
        prepareCalendarView()

        output.viewIsReady()
    }
    
    func prepareDataSource() {
        dataSource = WorkTimeDataSource()
        dataSource?.setDateFormat(format: "E")
    }
    
    func prepareCalendarView() {
        view.addSubview(viewCalendar)
        
        constrain(view, viewCalendar) {
            (view, calendar) in
            calendar.edges == view.edges
        }
        
        viewCalendar.delegate = self
    }
}

extension WorkTimeViewController: WorkTimeViewInput {
    
    func update(records: [WorkTimeRecordGroup]) {
        dataSource?.update(data: records)
        viewCalendar.reloadData()
    }
}

extension WorkTimeViewController: WorkTimeCalendarDelegate {
    
    func numberOfIndicators() -> Int {
        return dataSource?.intervalQuantity ?? 0
    }
    
    func value(for indicator: Int) -> TimeInterval {
        return dataSource?.timeAmountForInterval(at: indicator) ?? 0
    }
    
    func maximumValue(for indicator: Int) -> TimeInterval {
        return dataSource?.maximalTimeAmountForInterval(at: indicator) ?? 0
    }
    
    func labelText(for indicator: Int) -> String {
        return dataSource?.representativeDateForInterval(at: indicator) ?? ""
    }
}
