//
//  WorkTimeIndicatorView.swift
//  StudyOOP
//
//  Created by Filipp Lebedev on 07.09.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import UIKit
import Cartography

class WorkTimeIndicatorView: UIView {

    private var timeValue: TimeInterval = 0
    private var maxTimeValue: TimeInterval = 0
    private var lineColor: UIColor = .blue
    private var containerColor: UIColor = .gray
    private var containerHeight: CGFloat = 300
    
    private var viewContainer = UIView()
    private var viewLine = UIView()
    private var constraintLineHeight = NSLayoutConstraint()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        drawInitial()
    }
    
    init(frame: CGRect, time: TimeInterval, maxTime: TimeInterval) {
        super.init(frame: frame)
        drawInitial()
        setTime(time)
        setMaxTime(maxTime)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setTime(_ value: TimeInterval) {
        timeValue = value
        updateView()
    }
    
    func setMaxTime(_ value: TimeInterval) {
        maxTimeValue = value
        updateView()
    }
    
    func setLineColor(_ color: UIColor) {
        lineColor = color
        updateColors()
    }
    
    func drawInitial() {
        addSubview(viewContainer)
        viewContainer.addSubview(viewLine)
        
        constrain(self, viewContainer, viewLine) {
            (view, container, line) in
            container.edges == view.edges
            line.bottom == container.bottom
            line.left == container.left
            line.right == container.right
            constraintLineHeight = ( line.height == 0 )
        }
        
        updateColors()
    }
    
    func updateView() {
        guard maxTimeValue > 0 else { return }
        constraintLineHeight.constant = CGFloat((Double(containerHeight) / maxTimeValue) * timeValue)
    }
    
    func updateColors() {
        viewLine.backgroundColor = lineColor
        viewContainer.backgroundColor = containerColor
    }
}
