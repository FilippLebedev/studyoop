//
//  WorkTimeCalendarView.swift
//  StudyOOP
//
//  Created by Filipp Lebedev on 07.09.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import UIKit
import Cartography

class WorkTimeCalendarView: UIView {

    weak var delegate: WorkTimeCalendarDelegate? = nil
    
    private var indicators = [WorkTimeIndicatorView]()
    private var labels = [WorkTimeDateLabel]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func reloadData() {
        reset()
        
        let numberOfIndicators = delegate?.numberOfIndicators() ?? 0
        
        for i in 0..<numberOfIndicators {
            let indicatorValue = delegate?.value(for: i) ?? 0
            let maxValue = delegate?.maximumValue(for: i) ?? 0
            let newIndicator = WorkTimeIndicatorView(frame: CGRect.null, time: indicatorValue, maxTime: maxValue)
            indicators.append(newIndicator)
            
            let labelText = delegate?.labelText(for: i) ?? ""
            let newLabel = WorkTimeDateLabel(text: labelText)
            labels.append(newLabel)
        }
        
        drawIndicators()
        drawLabels()
    }
    
    private func drawIndicators() {
        for (index, indicator) in indicators.enumerated() {
            addSubview(indicator)
            
            let leftMargin = CGFloat(50 * index + 50)
            
            constrain(self, indicator) {
                (view, indicator) in
                indicator.left == view.left + leftMargin
                indicator.height == 300
                indicator.bottom == view.bottom - 50
                indicator.width == 20
            }
        }
    }
    
    private func drawLabels() {
        for (index, label) in labels.enumerated() {
            addSubview(label)
            
            if index < indicators.count {
                let indicator = indicators[index]
                constrain(indicator, label) {
                    (indicator, label) in
                    label.top == indicator.bottom + 5
                    label.centerX == indicator.centerX
                }
            }
        }
    }
    
    func reset() {
        indicators = []
        labels = []
        clearView()
    }
    
    func clearView() {
        for s in subviews {
            s.removeFromSuperview()
        }
    }
}

protocol WorkTimeCalendarDelegate: class {
    func numberOfIndicators() -> Int
    func value(for indicator: Int) -> TimeInterval
    func maximumValue(for indicator: Int) -> TimeInterval
    func labelText(for indicator: Int) -> String
}
