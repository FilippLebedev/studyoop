//
//  WorkTimeWorkTimeViewOutput.swift
//  StudyOOP
//
//  Created by Filipp Lebedev on 06/09/2018.
//  Copyright © 2018 FilippLebedev. All rights reserved.
//

protocol WorkTimeViewOutput {

    /**
        @author Filipp Lebedev
        Notify presenter that view is ready
    */

    func viewIsReady()
}
