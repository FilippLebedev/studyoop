//
//  WorkTimeDateLabel.swift
//  StudyOOP
//
//  Created by Filipp Lebedev on 07.09.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import UIKit

class WorkTimeDateLabel: UILabel {
    
    init(text: String) {
        super.init(frame: CGRect.null)
        textAlignment = .center
        self.text = text
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
