//
//  WorkTimeWorkTimeInteractor.swift
//  StudyOOP
//
//  Created by Filipp Lebedev on 06/09/2018.
//  Copyright © 2018 FilippLebedev. All rights reserved.
//

import Foundation

class WorkTimeInteractor: WorkTimeInteractorInput {

    weak var output: WorkTimeInteractorOutput!

    func setup() {
        
        // Data for example
        
        let timeRange: UInt32 = 86400 * 7

        var records = [WorkTimeRecord]()
        
        for _ in 0...50 {
            let randomDateDifference = TimeInterval(arc4random_uniform(timeRange))
            let randomDuration = TimeInterval(arc4random_uniform(7200))
            records.append(WorkTimeRecord(startTime: Date().addingTimeInterval(-randomDateDifference) , duration: randomDuration))
        }
        
        output.update(records: records)
    }
}
