//
//  WorkTimeInterval.swift
//  StudyOOP
//
//  Created by Filipp Lebedev on 06.09.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation

class WorkTimeInterval {
    
    var minimalDate: Date
    var maximalDate: Date
    
    var maximalDuration: TimeInterval {
        return maximalDate.timeIntervalSince(minimalDate)
    }
    
    init(minimalDate: Date, maximalDate: Date) {
        self.minimalDate = minimalDate
        self.maximalDate = maximalDate
    }
}

class WorkTimeIntervalRange {
    
    var intervals: [WorkTimeInterval] = []

    init(by template: WorkTimeIntervalRangeTemplate, startTime: Date) {
        for i in 0..<template.intervalQuantity {
            let minimalDate = startTime.addingTimeInterval(Double(i) * template.intervalDuration)
            let maximalDate = minimalDate.addingTimeInterval(template.intervalDuration)
            let newInterval = WorkTimeInterval(minimalDate: minimalDate, maximalDate: maximalDate)
            intervals.append(newInterval)
        }
    }
}

class WorkTimeIntervalRangeTemplate {
    
    var intervalDuration: TimeInterval
    var intervalQuantity: Int
    
    init(intervalDuration: TimeInterval, intervalQuantity: Int) {
        self.intervalDuration = intervalDuration
        self.intervalQuantity = intervalQuantity
    }
}
