//
//  StartStartRouter.swift
//  StudyOOP
//
//  Created by Filipp Lebedev on 06/09/2018.
//  Copyright © 2018 FilippLebedev. All rights reserved.
//

import UIKit

class StartRouter: StartRouterInput {

    func openTask(by name: String) {
        
        let moduleConfigurator = Manager.shared.moduleConfigurator
        var module: UIViewController? = nil
        
        switch name {
        case "WorkTime": module = moduleConfigurator?.createWorkTime()
        default:
            break
        }
        
        if let module = module {
            Manager.shared.pushViewController(module)
        }
    }
}
