//
//  StartStartPresenter.swift
//  StudyOOP
//
//  Created by Filipp Lebedev on 06/09/2018.
//  Copyright © 2018 FilippLebedev. All rights reserved.
//

class StartPresenter: StartModuleInput, StartViewOutput, StartInteractorOutput {

    weak var view: StartViewInput!
    var interactor: StartInteractorInput!
    var router: StartRouterInput!

    func viewIsReady() {

    }
    
    func taskSelected(task: String) {
        router.openTask(by: task)
    }
}
