//
//  StartStartViewInput.swift
//  StudyOOP
//
//  Created by Filipp Lebedev on 06/09/2018.
//  Copyright © 2018 FilippLebedev. All rights reserved.
//

protocol StartViewInput: class {

    /**
        @author Filipp Lebedev
        Setup initial state of the view
    */

    func setupInitialState()
}
