//
//  StartTaskTableViewCell.swift
//  StudyOOP
//
//  Created by Filipp Lebedev on 07.09.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import UIKit

class StartTaskTableViewCell: UITableViewCell {
    
    @IBOutlet weak private var labelTitle: UILabel?
    
    var title: String {
        get { return labelTitle?.text ?? "" }
        set { labelTitle?.text = newValue }
    }
}
