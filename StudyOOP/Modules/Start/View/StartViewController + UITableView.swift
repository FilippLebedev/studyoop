//
//  StartViewController + UITableView.swift
//  StudyOOP
//
//  Created by Filipp Lebedev on 06.09.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

import Foundation
import UIKit

extension StartViewController {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cellTask", for: indexPath) as? StartTaskTableViewCell else {
            return UITableViewCell()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? StartTaskTableViewCell else { return }
        cell.title = tasks[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        output.taskSelected(task: tasks[indexPath.row])
    }
}
