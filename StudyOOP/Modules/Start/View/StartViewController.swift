//
//  StartStartViewController.swift
//  StudyOOP
//
//  Created by Filipp Lebedev on 06/09/2018.
//  Copyright © 2018 FilippLebedev. All rights reserved.
//

import UIKit

class StartViewController: UIViewController, StartViewInput, UITableViewDelegate, UITableViewDataSource {

    var output: StartViewOutput!
    
    var tasks: [String] = [
        "WorkTime"
    ]

    @IBOutlet weak var tableView: UITableView?
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView?.delegate = self
        tableView?.dataSource = self
        
        output.viewIsReady()
    }

    // MARK: StartViewInput
    func setupInitialState() {
    }
}
